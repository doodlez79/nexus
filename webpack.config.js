
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const TerserPlugin = require('terser-webpack-plugin');
const autoprefixer = require('autoprefixer');
var HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");


module.exports = {
    entry: './src/app.js',
    output: {
        path: __dirname + "/dist/",
        filename: "bundle.js",
        chunkFilename: '[name].js'
    },
    watchOptions: {
        poll: 1000 // Check for changes every second
    },
    devServer: {
        contentBase: __dirname,
        hot: true,
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader'
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: [
                                    autoprefixer({
                                        browsers:['ie >= 8', 'last 4 version']
                                    })
                                ],
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'sass-loader'
                        },
                    ],
                })
            },
            {
                test:/\.pug$/,
                loader: 'pug-loader',
                options: {
                    pretty: true
                }
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    'url-loader?limit=10000',
                    'img-loader'
                ]
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                use: [
                    {
                        loader: 'file-loader?name=./font/[name].[ext]'
                    },
                ]
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    {
                        loader: 'file-loader?name=./img/[name].[ext]'
                    },
                ]
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    'file-loader',
                    {
                        loader: 'file-loader?name=./img/[name].[ext]',
                        options: {
                            bypassOnDebug: true, // webpack@1.x
                            disable: true, // webpack@2.x and newer
                        },
                    },
                ],
            },
        ]
    },

  plugins: [
    new HtmlWebpackPlugin({
      template: __dirname + '/src/pug/index.pug',
      filename: __dirname + '/dist/index.html'
    }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/visitPage.pug',
          filename: __dirname + '/dist/visitPage.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/direction.pug',
          filename: __dirname + '/dist/direction.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/spec.pug',
          filename: __dirname + '/dist/spec.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/catalog.pug',
          filename: __dirname + '/dist/catalog.html'
      }),

      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/resultRearch.pug',
          filename: __dirname + '/dist/resultRearch.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/detailPage.pug',
          filename: __dirname + '/dist/detailPage.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/404.pug',
          filename: __dirname + '/dist/404.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/action.pug',
          filename: __dirname + '/dist/action.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/delivery.pug',
          filename: __dirname + '/dist/delivery.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/contacts.pug',
          filename: __dirname + '/dist/contacts.html'
      }),
      new HtmlWebpackPlugin({
          template: __dirname + '/src/pug/about.pug',
          filename: __dirname + '/dist/about.html'
      }),
      new CopyPlugin({
          patterns: [
              { from: "src/assets", to: "img" },
          ],
      }),

    new ExtractTextPlugin('[name].css'),
    new BrowserSyncPlugin({
      // browse to http://localhost:3000/ during development,
      // ./public directory is being served
      host: 'localhost',
      port: 8080,
      server: { baseDir: __dirname + "/dist/" },
    })
    ],
    optimization: {
        minimizer: [
          new TerserPlugin(),
          new OptimizeCSSAssetsPlugin({})
        ]
    }
};
