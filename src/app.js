'use strict';

const $ = require("jquery");

window.jQuery = $;

if ($) {
    require('bootstrap');
    require('owl.carousel');
    require('slideout');
    require("./js/main.js");
    require("./scss/required.scss");
}
