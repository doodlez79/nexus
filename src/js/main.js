import Slideout from "slideout/dist/slideout.min"

jQuery(document).ready(() => {


    const btn = jQuery(".btn-search")
    const blockSearch = jQuery(".search-block-desktop")
    const blockSearchMobile = jQuery(".mobile-search-block")

    const orderCall = jQuery(".order-call")

    if (orderCall) {
        orderCall.hide();
    }

    jQuery(window).scroll(function() {
        if (jQuery(document).scrollTop() > 600) {
            console.log("123")
            if (orderCall) {
                orderCall.fadeIn('slow');
            }
        }
        else {
            if (orderCall) {
                orderCall.fadeOut('slow');
            }

        }
    });

    btn.on("click", () => {
        let currentBlock = window.innerWidth > 992 ? blockSearch : blockSearchMobile

        console.log(window.innerWidth > 992 ? blockSearch : blockSearchMobile)
        if (currentBlock.hasClass("d-flex")) {
            currentBlock.removeClass("d-flex")
            currentBlock.addClass("d-none")
        } else {
            currentBlock.removeClass("d-none")
            currentBlock.addClass("d-flex")
        }
    })
        if (document.getElementById('panel') && document.getElementById('menu')) {
            var slideout = new Slideout({
                'panel': document.getElementById('panel'),
                'menu': document.getElementById('menu'),
                'padding': window.innerWidth / 2,
                'tolerance': 70,
                "side": "right"
            });

            // Toggle button
            document.querySelector('.btn-mobile-toggle').addEventListener('click', function() {
                slideout.toggle();
            });
        }


    jQuery('.owl-carousel').owlCarousel({
        loop:true,
        margin:24,
        responsiveClass:true,
        navContainerClass: "nexus-carousel-nav",
        navText: [`<svg width="29" height="16" viewBox="0 0 29 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path opacity="0.7" d="M0.292892 7.29289C-0.0976315 7.68342 -0.0976315 8.31658 0.292892 8.70711L6.65685 15.0711C7.04738 15.4616 7.68054 15.4616 8.07107 15.0711C8.46159 14.6805 8.46159 14.0474 8.07107 13.6569L2.41421 8L8.07107 2.34315C8.46159 1.95262 8.46159 1.31946 8.07107 0.928932C7.68054 0.538408 7.04738 0.538408 6.65685 0.928932L0.292892 7.29289ZM29 7L1 7V9L29 9V7Z" fill="#1B1717"/>
            </svg>
            `,`<svg width="29" height="16" viewBox="0 0 29 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path opacity="0.7" d="M28.7071 8.70711C29.0976 8.31659 29.0976 7.68342 28.7071 7.2929L22.3431 0.928934C21.9526 0.53841 21.3195 0.53841 20.9289 0.928934C20.5384 1.31946 20.5384 1.95262 20.9289 2.34315L26.5858 8L20.9289 13.6569C20.5384 14.0474 20.5384 14.6805 20.9289 15.0711C21.3195 15.4616 21.9526 15.4616 22.3431 15.0711L28.7071 8.70711ZM-8.74228e-08 9L28 9L28 7L8.74228e-08 7L-8.74228e-08 9Z" fill="#1B1717"/>
            </svg>
            `],
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:3,
                nav:false
            },
            1000:{
                items:4,
                nav:true,
                loop:true
            }
        }
    })
console.log(typeof ymaps, "ymaps")
    if (typeof ymaps !== "undefined") {
        ymaps.ready(function () {
            var myMap = new ymaps.Map("map", {
                center: [55.76, 37.64],
                zoom: 10
            });

            var myPlacemark = new ymaps.Placemark([55.76, 37.56], {}, {
                iconLayout: 'default#image',
                iconImageHref: 'img/marker.svg',
                iconImageSize: [30, 42],
                iconImageOffset: [-3, -42]
            });
            myMap.geoObjects.add(myPlacemark);
        });
    }





    const allSmallPhotosDetail = jQuery(".js-detail-smallImg")
    const bigPhotosDetail = jQuery(".js-detail-bigImg")
    if (allSmallPhotosDetail && bigPhotosDetail) {
        const imgBig = bigPhotosDetail.find("img")[0]
        allSmallPhotosDetail.each((index, value) =>  {

            jQuery(value).click(function () {
                jQuery(this).addClass("active")
               const imgPath = jQuery(this).find("img")[0].src
                jQuery(imgBig).attr("src", imgPath)
                deleteActiveClass(this)
            })

        })
        function deleteActiveClass(elem) {
            allSmallPhotosDetail.each((index, value) =>  {
                if (value !== elem && jQuery(elem).hasClass("active")) {
                    jQuery(value).removeClass("active")
                }
            })
        }
    }
})
